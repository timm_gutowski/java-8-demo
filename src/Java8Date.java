import com.xmas.DaysUntilChristmasCounter;
import org.junit.jupiter.api.Test;

import java.text.SimpleDateFormat;
import java.time.Clock;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.MonthDay;
import java.time.Period;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjusters;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class Java8Date
{

    /**
     * https://infiniteundo.com/post/25326999628/falsehoods-programmers-believe-about-time
     *
     * There are always 24 hours in a day.
     * Months have either 30 or 31 days.
     * Years have 365 days.
     * February is always 28 days long.
     * Any 24-hour period will always begin and end in the same day (or week, or month).
     * A week always begins and ends in the same month.
     * A week (or a month) always begins and ends in the same year.
     * The machine that a program runs on will always be in the GMT time zone.
     * Ok, that’s not true. But at least the time zone in which a program has to run will never change.
     * Well, surely there will never be a change to the time zone in which a program hast to run in production.
     * ...
     */

    /**
     * Calculate the days until Christmas Eve with the old API
     */
    @Test
    public void days_until_christmas_eve()
    {
        final SimpleDateFormat dateFormat = new SimpleDateFormat( "yyyy-MM-dd'T'HH:mm:ss.SSSZ" );
        final int msPerDay = 1000 * 60 * 60 * 24;

        // Date is not only a date, it's a specific instant in time, with millisecond precision.
        // This represents midnight, local time, at the beginning of the day specified by the year, month, and date arguments.
        final Date today = new Date( 2018, 11, 6 );
        // The Date constructor above is deprecated since Java 1.1, we should use the Calendar instead.
        final Date christmasEve = new GregorianCalendar( 2018, 12, 24 ).getTime();

        final int daysUntilChristmas = ( int ) ( ( christmasEve.getTime() - today.getTime() ) / msPerDay );

        final String errorMessage = String.format( "Wrong number of days between %s and %s.",
                                                   dateFormat.format( today ),
                                                   dateFormat.format( christmasEve ) );
        // This will fails, because our assumptions were wrong:
        // new Date( 2018, 11, 6 ) = Dec 6th 3918
        // new GregorianCalendar( 2018, 12, 24 ) = Jan 24th 2019
        assertEquals( 48, daysUntilChristmas, errorMessage );
    }

    /**
     * Now we're smarter. We know that years are 1900-based and months are 0-based. What can go wrong?
     */
    @Test
    public void days_between_two_days()
    {
        final SimpleDateFormat dateFormat = new SimpleDateFormat( "yyyy-MM-dd'T'HH:mm:ss.SSSZ" );
        final int msPerDay = 1000 * 60 * 60 * 24;

        final Date startOfDay = new Date( 118, 9, 28 );
        final Date endOfDay = new Date( 118, 9, 28, 23, 59, 59 );

        final int daysBetween = ( int ) ( ( endOfDay.getTime() - startOfDay.getTime() ) / msPerDay );

        final String errorMessage = String.format( "Number of days between %s and %s should be 0, " +
                                                           "because it's the same day.",
                                                   dateFormat.format( startOfDay ),
                                                   dateFormat.format( endOfDay ) );
        // This should fail, because our assumption was wrong: one day can have more than 24 hours
        assertEquals( 0, daysBetween, errorMessage );
    }

    // immutable and thread-safe!
    DateTimeFormatter _dateFormatter = DateTimeFormatter.ISO_LOCAL_DATE;

    DateTimeFormatter _dateTimeFormatter = DateTimeFormatter.ISO_LOCAL_DATE_TIME;

    /**
     * Use LocalDate, which accepts 2018 as 2018 and 11 as November.
     * We don't need time or a timezone.
     */
    @Test
    public void days_until_christmas_eve_with_new_API()
    {
        LocalDate today = LocalDate.of( 2018, 11, 6 );
        // You can use MONTH instead of numbers
        LocalDate christmasEve = LocalDate.of( 2018, Month.DECEMBER, 24 );

        final long daysUntilChristmas = ChronoUnit.DAYS.between( today, christmasEve );

        final String errorMessage = String.format( "Wrong number of days between %s and %s.",
                                                   _dateFormatter.format( today ),
                                                   _dateFormatter.format( christmasEve ) );
        assertEquals( 48, daysUntilChristmas, errorMessage );
    }

    /**
     * What if a class does something based on the current date? How can we test it?
     */
    @Test
    public void format_days_until_christmas_eve_with_new_API()
    {
        LocalDate dec23 = LocalDate.of( 2018, 12, 23 );
        // a "clock" which always returns the same instant
        final Clock fixedClock = Clock.fixed( LocalDateTime.of( dec23, LocalTime.MIDNIGHT )
                                                           .toInstant( ZoneOffset.UTC ),
                                              ZoneOffset.UTC );

        DaysUntilChristmasCounter counter = new DaysUntilChristmasCounter( fixedClock );
        final long daysUntilChristmas = counter.days();

        assertEquals( 1, daysUntilChristmas,
                      "It should be one day from Dec 23 to Christmas Eve." );
    }

    /**
     * Duration is time based. 1 day = 24 hours
     */
    @Test
    public void add_duration()
    {
        final ZoneId zoneIdEurope = ZoneId.of( "Europe/Berlin" );
        final LocalDate oct28 = LocalDate.of( 2018, 10, 28 );
        final LocalDateTime beforeTimeSwitch = LocalDateTime.of( oct28, LocalTime.MIDNIGHT );
        final ZonedDateTime zonedDateTimeBeforeSwitch = ZonedDateTime.of( beforeTimeSwitch, zoneIdEurope );

        final ZonedDateTime dayPlus24Hours = zonedDateTimeBeforeSwitch.plus( Duration.ofDays( 1 ) );

        assertEquals( oct28, dayPlus24Hours.toLocalDate(),
                      "Duration of 1 day is 24 hours, but Oct 28 has 25 hours due to time switch, " +
                              "so the date should be the same." );
    }

    /**
     * Period is date-based. Adding one day will add a "conceptual" day.
     */
    @Test
    public void add_period()
    {

        final ZoneId timezoneBerlin = ZoneId.of( "Europe/Berlin" );
        final LocalDate oct28 = LocalDate.of( 2018, Month.OCTOBER, 28 );
        final LocalDateTime beforeTimeSwitch = LocalDateTime.of( oct28, LocalTime.MIDNIGHT );
        final ZonedDateTime zonedDateTimeBeforeSwitch = ZonedDateTime.of( beforeTimeSwitch, timezoneBerlin );

        final ZonedDateTime dayPlusOneDay = zonedDateTimeBeforeSwitch.plus( Period.ofDays( 1 ) );

        assertNotEquals( oct28, dayPlusOneDay.toLocalDate(), "One day (which can be more than 24 hours), " +
                "has been added, so the dates should be different." );
    }
}
