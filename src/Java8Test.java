import org.apache.log4j.Logger;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class Java8Test {

    private final Person merkel = new Person("Angela", "Merkel");
    private final Person schulz = new Person("Martin", "Schulz");
    private final Person schmitt = new Person("Martin", "Schmitt");

    private final Logger logger = Logger.getLogger(Java8Test.class);


    /**
     * Erzeugt eine Map aus einer Personenliste (oder bspw. flsIdent -> Material)
     */
    @Test
    public void collectorsToMapMitUniqueKey() {
        List<Person> personen = Arrays.asList(merkel, schulz);
        //Person::getVorname => Method reference, dasselbe wie person -> person.getVorname()
        // Function.identity() = t -> t
        Map<String, Person> vornameZuPerson = personen.stream()
                .collect(Collectors.toMap(Person::getVorname, Function.identity()));

        assertEquals(merkel, vornameZuPerson.get("Angela"));
        assertEquals(schulz, vornameZuPerson.get("Martin"));
    }

    /**
     * Beispiel dafür, wie man teure Operationen im Logging verhindern kann.
     */
    @Test
    public void testLogging(){
        // Hier wird die teure Operation aufgerufen, auch wenn das Log-Level auf Info steht.
        logger.debug("Ohne Prüfung: "+teureStringOperation());

        /*
        So vermeidet man den Aufruf normalerweise. Typische Fehlerquelle: auf isDebugEnabled prüfen, aber mit Info loggen.
         */
        if (logger.isDebugEnabled()) {
            logger.debug("Mit Prüfung: "+teureStringOperation());
        }

        // die teure Operation wird nicht aufgerufen, weil der Lambda-Ausdruck erst bei Bedarf ausgewertet wird.
        logDebug(() -> "Hallo "+teureStringOperation());
    }

    private void logDebug(Supplier<String> nachricht) {
        logger.info("Aufruf aus logDebug.");
        if (logger.isDebugEnabled()) {
            logger.debug(nachricht.get());
        }
    }

    /**
     * collect(Collectors.toMap... erwartet eindeutige Keys
     */
    @Test
    public void collectorsToMapMitDoppeltemKey() {
        List<Person> personen = Arrays.asList(merkel, schulz, schmitt);

        Assertions.assertThrows(IllegalStateException.class,
                () -> personen.stream()
                        .collect(Collectors.toMap(Person::getVorname, Function.identity())));
    }

    /**
     * Mit collect(Collectors.groupingBy(... kann man mehrere Werte für einen Key bekommen.
     */
    @Test
    public void collectorsToMap() {
        List<Person> personen = Arrays.asList(merkel, schulz, schmitt);

        Map<String, List<Person>> group = personen.stream()
                .collect(Collectors.groupingBy(Person::getVorname));

        assertEquals(2, group.get("Martin").size() );
    }

    @Test
    public void lazyStreams(){
        List<Person> personen = Arrays.asList(merkel, schulz, schmitt);
        long result = personen.stream()
                .map(this::teurerAPICall)
                .filter(vorname -> vorname.equals("Martin"))
                .limit(1L)
                .count();
        assertEquals(1L, result);
    }

/*    @Test
    public void kaputtesExceptionHandling(){
        List<Person> personen = Arrays.asList(merkel, schulz, schmitt);

        long result = personen.stream()
                .map(person -> methodeMitCheckedException(person))
                .count();
        Assertions.assertEquals(1L, result);
    }*/

    @Test
    public void exceptionHandling(){
        List<Person> personen = Arrays.asList(merkel, schulz, schmitt);
        Function<Person, String> wrapper = person -> {
            try {
                return methodeMitCheckedException(person);
            } catch (Exception e) {
                throw new RuntimeException(e.getMessage(), e);
            }
        };

        long result = personen.stream()
                .map(person -> wrapper)
                .count();

        assertEquals(3L, result);
    }

    @Test
    public void testOptional(){
        Optional<Boolean> booleanValue = Optional.ofNullable(potentielleNullMethode(true));
        // Variante 1: mit isPresent()
        if (booleanValue.isPresent()) {
            assertTrue(booleanValue.get());
        }
        else{
            boolean value = true;
            assertTrue(value);
        }

        //Variante 2: mit default-Wert
        assertTrue(booleanValue.orElse(Boolean.TRUE));

    }

    private Boolean potentielleNullMethode(boolean returnNull) {
        return returnNull ? null : Boolean.TRUE;
    }

    private String methodeMitCheckedException(Person person) throws Exception {
        throw new Exception("Checked Exception");
    }

    private String teurerAPICall(Person person) {
        System.out.println("Verarbeite " + person.getVorname() + " " + person.getNachname());
        return person.getVorname();
    }

    private String teureStringOperation() {
        logger.info("Aufruf von teureStringOperation().");
        return "Welt";
    }

    private class Person {
        private final String _nachname;
        private final String _vorname;

        Person(String vorname, String nachname) {
            _vorname = vorname;
            _nachname = nachname;
        }

        String getVorname() {
            return _vorname;
        }

        String getNachname() {
            return _nachname;
        }

        @Override
        public String toString() {
            return "Person{" +
                    "_nachname='" + _nachname + '\'' +
                    ", _vorname='" + _vorname + '\'' +
                    '}';
        }
    }
}
