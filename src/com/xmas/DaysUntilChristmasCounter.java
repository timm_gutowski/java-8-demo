package com.xmas;

import java.time.Clock;
import java.time.LocalDate;
import java.time.Month;
import java.time.temporal.ChronoUnit;

public class DaysUntilChristmasCounter
{

    private Clock _clock;

    public DaysUntilChristmasCounter()
    {
        this( Clock.systemDefaultZone() );
    }

    public DaysUntilChristmasCounter( final Clock clock )
    {
        _clock = clock;
    }

    public long days()
    {
        LocalDate today = LocalDate.now( _clock );
        LocalDate christmasEve = LocalDate.of( 2018, Month.DECEMBER, 24 );

        return ChronoUnit.DAYS.between( today, christmasEve );
    }
}
